using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Config", menuName = "Config", order = 1)]
[Serializable]
public class Config : ScriptableObject
{
    [Header("Player Settings")]
    [Space]
    public float scaleSpeed;
    public float moveSpeed;

    public float startForce = 1f;
    public float forceStep;

    [Header("Player Settings")]
    [Space]
    public Material baseEnemyMaterial;
    public Material inZoneEnemyMaterial;
}
