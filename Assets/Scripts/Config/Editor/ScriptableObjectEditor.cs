﻿using UnityEditor;
using UnityEngine;

namespace Submodules.MaverickUtils.Config.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ScriptableObject), true)]
    public class ScriptableObjectEditor : UnityEditor.Editor
    {
    }
}