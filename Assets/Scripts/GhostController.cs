﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GhostController : MonoBehaviour
{
    public LayerMask enemyMask;

    private float force;
    private float forceStep;

    private bool canCheck = false;

    private Collider[] enemysInZone;

    private Material baseEnemyMaterial;
    private Material inZoneEnemyMaterial;

    private List<Renderer> lastEnemyInZone;

    private void Start()
    {
        force = 1f;
        lastEnemyInZone = new List<Renderer>();

        InitParams();
    }

    private void InitParams()
    {
        force = ConfigInstance.Instance.config.startForce;
        forceStep = ConfigInstance.Instance.config.forceStep * Time.deltaTime;
        baseEnemyMaterial = ConfigInstance.Instance.config.baseEnemyMaterial;
        inZoneEnemyMaterial = ConfigInstance.Instance.config.inZoneEnemyMaterial;
    }

    private void Update()
    {
        if (canCheck)
        {
            enemysInZone = Physics.OverlapSphere(transform.position, force, enemyMask);

            CheckOldEnemys();
            lastEnemyInZone = new List<Renderer>();
            CheckNewEnemys();
        }
    }

    private void CheckNewEnemys()
    {
        for (int i = 0; i < enemysInZone.Length; i++)
        {
            var rend = enemysInZone[i].GetComponent<Renderer>();
            rend.material = inZoneEnemyMaterial;
            lastEnemyInZone.Add(rend);
        }
    }

    private void CheckOldEnemys()
    {
        for (int i = 0; i < lastEnemyInZone.Count; i++)
        {
            if (enemysInZone.Where(
                el => el.name == lastEnemyInZone[i].name).Count() < 1)
            {
                lastEnemyInZone[i].material = baseEnemyMaterial;
            }
        }
    }

    public void AddForce()
    {
        force += forceStep;
    }

    public void StartCheckEnemy()
    {
        canCheck = true;
        force = ConfigInstance.Instance.config.startForce;
    }

    public void StopCheckEnemy()
    {
        canCheck = false;

        for (int i = 0; i < lastEnemyInZone.Count; i++)
        {
            Destroy(lastEnemyInZone[i].gameObject);
        }

        lastEnemyInZone = new List<Renderer>();
    }
}
