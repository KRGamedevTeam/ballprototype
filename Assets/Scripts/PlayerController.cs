﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{    
    public GhostController targetBall;

    [HideInInspector]
    public PlayerState playerState = PlayerState.WAIT;

    private float scaleSpeed;
    private float moveSpeed;

    private Vector3 direction = Vector3.up;
    private Vector3 lastScale;

    private PlayerLineRender line;

    private void Start()
    {
        lastScale = transform.localScale;

        InitParams();
    }

    private void Update()
    {
        if(playerState == PlayerState.MOVE)
        {
            transform.position = Vector3.MoveTowards(transform.position,
            targetBall.transform.position, Time.deltaTime * moveSpeed*2);

            line.UpdateLine(transform.position, targetBall.transform.position);

            if (Vector3.Distance(transform.position, targetBall.transform.position) < 0.01f)
            {
                playerState = PlayerState.WAIT;
                line.DisableLine();
            }
        }

        if (transform.localScale.x <= 0)
        {
            Debug.Log("lose");
            StartCoroutine(waitToRestart());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            Debug.Log("lose");
            StartCoroutine(waitToRestart());
        }

        if (other.CompareTag("Finish"))
        {
            Debug.Log("win");
            StartCoroutine(waitToRestart());
        }
    }

    IEnumerator waitToRestart()
    {
        playerState = PlayerState.LOSE;
        GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void InitParams()
    {
        scaleSpeed = ConfigInstance.Instance.config.scaleSpeed;
        moveSpeed = ConfigInstance.Instance.config.moveSpeed;

        line = GetComponent<PlayerLineRender>();
    }

    public void StartTouch()
    {
        targetBall.StartCheckEnemy();
    }

    public void UpdateTouch()
    {
        transform.localScale = Vector3.MoveTowards(transform.localScale, Vector3.zero, Time.deltaTime * scaleSpeed);
        targetBall.transform.position = Vector3.MoveTowards(targetBall.transform.position, 
            targetBall.transform.position + direction * 10, Time.deltaTime * moveSpeed);

        targetBall.transform.localScale = Vector3.MoveTowards(targetBall.transform.localScale, 
            lastScale, Time.deltaTime * scaleSpeed);

        targetBall.AddForce();
    }

    public void EndTouch()
    {
        lastScale = transform.localScale;
        targetBall.StopCheckEnemy();

        playerState = PlayerState.MOVE;
        line.SetLine(transform.localScale.x);
    }
}
