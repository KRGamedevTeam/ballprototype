﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PlayerLineRender : MonoBehaviour
{
    private LineRenderer line;

    private void Start()
    {
        line = GetComponent<LineRenderer>();
        line.enabled = false;
    }

    public void SetLine(float width)
    {
        line.startWidth = line.endWidth = width;
        line.enabled = true;
    }

    public void UpdateLine(Vector3 start, Vector3 end)
    {
        line.SetPosition(0, start);
        line.SetPosition(1, end);
    }

    public void DisableLine()
    {
        line.enabled = false;
    }
}
