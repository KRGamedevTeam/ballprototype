using System;
using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
    private static T _instance;

    /// <summary>
    /// Instantiates GO with component if there is nothing on the scene;
    /// When you do not require scene configuration for it
    /// </summary>
    public static T CreateInstance()
    {
        var instance = Instance;
        if (instance == null)
        {
            GameObject go = new GameObject();
            go.name = typeof(T).ToString();
            _instance = go.AddComponent<T>();
        }

        return _instance;
    }

    /// <summary>
    ///  Returns instance or gets  it from the scene
    /// </summary>
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                var component = FindObjectOfType<T>();
                _instance = component;
                return _instance;
            }

            return _instance;
        }
        protected set { _instance = value; }
    }


    protected virtual void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Debug.LogError("An instance of this singleton already exists.", gameObject);
            Destroy(this);
        }
        else
        {
            _instance = (T)this;
        }
    }
}
