﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public PlayerController player;

    private void Start()
    {
        if (!player)
            player = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (!player)
            return;

        if (player.playerState == PlayerState.MOVE)
            return;

        if (Input.GetMouseButton(0))
        {
            player.UpdateTouch();
        }

        if (Input.GetMouseButtonUp(0))
        {
            player.EndTouch();
        }

        if (Input.GetMouseButtonDown(0))
        {
            player.StartTouch();
        }
    }
}
